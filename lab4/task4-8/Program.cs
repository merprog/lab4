﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Security;

class Program
{
    static void Main()
    {
        try
        {
            Console.WriteLine("Список автозавантажуваних програм та служб для поточного користувача:");
            ListStartupPrograms(Registry.CurrentUser, @"Software\Microsoft\Windows\CurrentVersion\Run");

            Console.WriteLine("Список автозавантажуваних програм та служб для всіх користувачів:");
            ListStartupPrograms(Registry.LocalMachine, @"Software\Microsoft\Windows\CurrentVersion\Run");

            AddStartupProgram();

            Console.WriteLine("Повторний список автозавантажуваних програм та служб для поточного користувача:");
            ListStartupPrograms(Registry.CurrentUser, @"Software\Microsoft\Windows\CurrentVersion\Run");

            Console.WriteLine("Список завдань у планувальнику задач:");
            ListScheduledTasks();

            // Отримання поточного каталогу
            string currentDirectory = Directory.GetCurrentDirectory();
            string backupFilePath = Path.Combine(currentDirectory, "BackupFile.reg");

            BackupRegistryKey(Registry.CurrentUser, @"Software\Microsoft\Windows\CurrentVersion\Run", backupFilePath);
        }
        catch (SecurityException ex)
        {
            Console.WriteLine($"Security Exception: {ex.Message}");
        }
        catch (UnauthorizedAccessException ex)
        {
            Console.WriteLine($"Unauthorized Access: {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }

    static void ListStartupPrograms(RegistryKey baseKey, string subKeyPath)
    {
        try
        {
            using (RegistryKey key = baseKey.OpenSubKey(subKeyPath))
            {
                if (key != null)
                {
                    string[] programs = key.GetValueNames();
                    foreach (var program in programs)
                    {
                        Console.WriteLine(program);
                    }
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine($"Неможливо відкрити розділ реєстру: {subKeyPath}");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while listing startup programs: {ex.Message}");
        }
    }

    static void AddStartupProgram()
    {
        try
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true))
            {
                key.SetValue("WinWord", @"C:\Program Files\Microsoft Office\root\Office16\WINWORD.EXE");
                Console.WriteLine("Програму додано до автозавантаження.");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while adding a startup program: {ex.Message}");
        }
    }

    static void ListScheduledTasks()
    {
        try
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree"))
            {
                if (key != null)
                {
                    string[] tasks = key.GetSubKeyNames();
                    foreach (var task in tasks)
                    {
                        Console.WriteLine(task);
                    }
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Неможливо відкрити розділ реєстру для планувальника задач.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while listing scheduled tasks: {ex.Message}");
        }
    }

    static void BackupRegistryKey(RegistryKey baseKey, string keyPath, string backupFilePath)
    {
        try
        {
            using (RegistryKey key = baseKey.OpenSubKey(keyPath))
            {
                if (key != null)
                {
                    using (StreamWriter sw = new StreamWriter(backupFilePath))
                    {
                        sw.WriteLine($"Windows Registry Editor Version 5.00");
                        sw.WriteLine();
                        ExportRegistryKeyRecursively(key, sw, keyPath);
                        Console.WriteLine($"Backup of '{keyPath}' has been created at '{backupFilePath}'.");
                    }
                }
                else
                {
                    Console.WriteLine($"Registry key '{keyPath}' not found.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while backing up the registry key: {ex.Message}");
        }
    }

    static void ExportRegistryKeyRecursively(RegistryKey key, StreamWriter sw, string keyPath)
    {
        sw.WriteLine($"[{keyPath}]");

        foreach (string valueName in key.GetValueNames())
        {
            object value = key.GetValue(valueName);
            sw.WriteLine($"\"{valueName}\"=\"{value}\"");
        }

        foreach (string subkeyName in key.GetSubKeyNames())
        {
            using (RegistryKey subkey = key.OpenSubKey(subkeyName))
            {
                string subkeyPath = $"{keyPath}\\{subkeyName}";
                ExportRegistryKeyRecursively(subkey, sw, subkeyPath);
            }
        }

        sw.WriteLine();
    }
}
